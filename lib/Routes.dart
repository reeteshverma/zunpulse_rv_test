import 'package:flutter/material.dart';
import 'main.dart';
import 'screens/Login.dart';
import 'screens/SignupPage.dart';
import 'theme/style.dart';

class Routes {
  var routes = <String, WidgetBuilder>{
    "/SignUp": (BuildContext context) => new SignupPage(),
  };

  Routes() {
    runApp(new MaterialApp(
      title: "Flutter Flat App",
      home: new MyApp(),
      theme: appTheme,
      routes: routes,
    ));
  }
}
