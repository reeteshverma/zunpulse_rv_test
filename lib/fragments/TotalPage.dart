import 'package:flutter/material.dart';

class TotalPage extends StatelessWidget {
  final int index;

  TotalPage(this.index);

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Text('TotalPage, index: $index'),
    );
  }
}