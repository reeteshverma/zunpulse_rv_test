import 'package:flutter/material.dart';
import 'dart:async';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

import '../model/incomingModal.dart';

class DayPage extends StatefulWidget {
  @override
  _DayPageState createState() => new _DayPageState();
}

class ClicksPerYear {
  final int year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

class _DayPageState extends State<DayPage> {
  int _counter = 0;
  // Initializing date and time
  String year = "", month = "", dd = "";
  DateTime _currentDate = new DateTime.now();
  String _toDate = "To", _fromDate = "From";
  String _value;
  List<String> _values = new List<String>();
  List<IncomingModal> incominglists= new List();
  //TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _values.addAll(["PP"]);
    _value = _values.elementAt(0);
    print("value of first position$_value");
       FirebaseDatabase.instance.reference()
       .child('incoming')
       .child("telemetry")
       .child("2018")
       .child("4")
       .child("16").
       once().then((onValue) {
     Map myMap = onValue.value;
     print("finally_database :$myMap ");
      var titles = myMap.values;
    List yearList = new List();
    for (var items in titles) {
      yearList.add(items['latest']);
     
    }
    //incominglists.add(new IncomingModal.fromSnapshot(snapshot));
      

      print("incoming_data_firebase${incominglists.length}");
     //print("yearlist_databas_latest :$yearList ");

     });
  }

//Create method for date picker
  Future<Null> _selectToDate(BuildContext context) async {
    final DateTime _pickedDate = await showDatePicker(
      context: context,
      initialDate:
          _currentDate, //for Current Date and these are the required parameters////
      firstDate: new DateTime(2016),
      lastDate: new DateTime(2019),
    );
    if (_pickedDate != null && _pickedDate != _currentDate) {
      print('Seleted Date :${_pickedDate.toString()}');
      setState(() {
        _toDate = _pickedDate
            .toString()
            .substring(0, 10); //for changing at a time select date
      });
    }
  }

//Create method for date picker
  Future<Null> _selectFromDate(BuildContext context) async {
    final DateTime _pickedDate = await showDatePicker(
      context: context,
      initialDate:
          _currentDate, //for Current Date and these are the required parameters////
      firstDate: new DateTime(2016),
      lastDate: new DateTime(2019),
    );
    if (_pickedDate != null && _pickedDate != _currentDate) {
      print('Seleted Date :${_pickedDate.toString()}');
      setState(() {
        _fromDate = _pickedDate.toString().substring(0, 10);
        //for changing at a time select date
        print('SetSatedate :${_fromDate.toString()}');
        List<String> dateParts = _fromDate.split("-");
        String year = dateParts[0];
        String month = dateParts[1];
        String dd = dateParts[2];
        print('Seleted Date :${year + "===="+ month+"=="+dd}');
      });
    }
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var data = [
      new ClicksPerYear(2016, 12, Colors.red),
      new ClicksPerYear(2017, 420, Colors.yellow),
      new ClicksPerYear(2018, _counter, Colors.green),
    ];

    var series = [
      new charts.Series(
        domainFn: (ClicksPerYear clickData, _) => clickData.year,
        measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
        colorFn: (ClicksPerYear clickData, _) => clickData.color,
        id: 'Clicks',
        data: data,
      ),
    ];
    var chart = new charts.LineChart(
      series,
      animate: true,
    );
    var chartWidget = new Padding(
      padding: new EdgeInsets.all(12.0),
      child: new SizedBox(
        height: 200.0,
        child: chart,
      ),
    );
    return new Scaffold(
      body: new Column(
        children: <Widget>[
          new Container(
            child: new Column(
              children: <Widget>[
                new Container(
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new DropdownButton(
                          value: _value,
                          items: _values.map((String value) {
                            return new DropdownMenuItem(
                              value: value,
                              child: new Text(
                                "$value",
                                style: new TextStyle(
                                    fontSize: 15.0, color: Colors.amberAccent),
                                textAlign: TextAlign.center,
                              ),
                            );
                          }).toList(),
                          onChanged: (String value) {
                            //_onChanged(value);
                          },
                        ),
                      ),
                      new Expanded(
                          child: new FlatButton(
                        child: new Text('${_fromDate}'),
                        onPressed: () {
                          _selectFromDate(context);
                        },
                      )),
                      new Expanded(
                          child: new FlatButton(
                        child: new Text('${_toDate}'),
                        onPressed: () {
                          _selectToDate(context);
                        },
                      )),
                      // new Expanded(
                      //   child: new Text('${_fromDate}',
                      //       style: new TextStyle(fontSize: 25.0)),
                      // ),
                      // new Flexible(
                      //   child: new Text('${_toDate}',
                      //       style: new TextStyle(fontSize: 25.0)),
                      // ),

                      // new Text('From',style: new TextStyle(fontSize: 15.0)),
                      // new Text('To',style: new TextStyle(fontSize: 15.0)),
                    ],
                  ),
                ),
                // new Container(
                //   padding: new EdgeInsets.all(10.0),
                //   child: new Row(
                //     children: <Widget>[
                //       // new Padding(
                //       //   padding: const EdgeInsets.only(right: 20.0),
                //       // ),
                //       new Expanded(
                //         child: new RaisedButton(
                //           onPressed: () {
                //             _selectFromDate(context);
                //           },
                //           child: new Text('Select from Date'),
                //         ),
                //       ),
                //       new Padding(
                //         padding: const EdgeInsets.only(left: 10.0),
                //       ),

                //       new Expanded(
                //         child: new RaisedButton(
                //           child: new Text('Select to Date'),
                //           onPressed: () {
                //             _selectToDate(context);
                //           },
                //         ),
                //       ),

                //       // new Text('From',style: new TextStyle(fontSize: 15.0)),
                //       // new Text('To',style: new TextStyle(fontSize: 15.0)),
                //     ],
                //   ),
                // ),
                // new Container(
                //   child: new Row(
                //     children: <Widget>[
                //        new TextFormField(
                //         decoration: new InputDecoration(
                //             labelText: "Please Select from Date", fillColor: Colors.white),
                //         keyboardType: TextInputType.datetime,
                //         onSaved: (String email) {
                //           // print("user_name:$email");
                //           // newUser.email = email;
                //         },
                //       ),
                //       new TextFormField(
                //         decoration: new InputDecoration(
                //             labelText: "Please Select to Date", fillColor: Colors.white),
                //         keyboardType: TextInputType.datetime,
                //         onSaved: (String email) {
                //           // print("user_name:$email");
                //           // newUser.email = email;
                //         },
                //       ),

                //     ],
                //   ),
                // ),
              ],
            ),
          ),
          chartWidget,
          new Container(
            
            child: new Column(
              
              children: <Widget>[
                new ListTile(
                  leading: new Image(image: new AssetImage("assets/images/moneyremaining.png"),),
                  title: new Text('Money Remaining',style: new TextStyle(fontSize: 25.0,color:Colors.grey[400] ),
                    
                  ),
                ),
         new ListTile(
                  leading: new Image(image: new AssetImage("assets/images/unitsremaining.png"),),
                  title: new Text('Units Remaining',style: new TextStyle(fontSize: 25.0,color:Colors.grey[400] ),
                    
                  ),
                ),
                  new ListTile(
                  leading: new Image(image: new AssetImage("assets/images/todayconsumption.png"),),
                  title: new Text('Todays Consumption',style: new TextStyle(fontSize: 25.0,color:Colors.grey[400] ),
                    
                  ),
                ),
                  new ListTile(
                  leading: new Image(image: new AssetImage("assets/images/lastrechargeamt.png"),),
                  title: new Text('Last Recharge Amount',style: new TextStyle(fontSize: 25.0,color:Colors.grey[400] ),
                    
                  ),
                ),
                  new ListTile(
                  leading: new Image(image: new AssetImage("assets/images/lastrechargedate.png"),),
                  title: new Text('Last Recharge Date',style: new TextStyle(fontSize: 25.0,color:Colors.grey[400] ),
                    
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      // floatingActionButton: new FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: new Icon(Icons.add),
      // ),
    );
  }
}
// class  extends StatelessWidget {
//   final int index;

//   DayPage(this.index);

//   @override
//   Widget build(BuildContext context) {
//     return new Center(
//       child: new Text('DayPage, index: $index'),
//     );
//   }
// }
