import 'package:flutter/material.dart';
import 'DayPage.dart';
import 'MonthPage.dart';
import 'SimpleTimeSeriesChart.dart';
import 'TotalPage.dart';
import 'YearPage.dart';
class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MyHomePage(),
      debugShowCheckedModeBanner:false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State createState() => new MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
  TabController _controller;
  int _index;

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 4, vsync: this);
    _index = 0;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      
      appBar: new TabBar(
            indicatorColor: Colors.amberAccent,
            unselectedLabelColor: Colors.amberAccent,
            labelColor: Colors.red,
        controller: _controller, tabs: <Tab>[
          new Tab(
            child:new Container(
              width: 180.0,
              color: Colors.red,
            child: new Center(child: new Text("Day",style: new TextStyle(fontSize: 22.0,color: Colors.white,),)),
          ),
          ),
           new Tab(
            child:new Container(
              width: 180.0,
              color: Colors.red,
            child: new Center(child: new Text("Month",style: new TextStyle(fontSize: 22.0,color: Colors.white,),)),
          ),
          ),
           new Tab(
            child:new Container(
              width: 180.0,
              color: Colors.red,
            child: new Center(child: new Text("Year",style: new TextStyle(fontSize: 22.0,color: Colors.white,),)),
          ),
          ),
           new Tab(
            child:new Container(
              width: 180.0,
              color: Colors.red,
            child: new Center(child: new Text("Total",style: new TextStyle(fontSize: 22.0,color: Colors.white,),)),
          ),
          ),

          // new Tab(text: "Month"),
          // new Tab(text: "Year"),
          // new Tab(text: "Total"),
        ]),
        
      
      body: new TabBarView(
        controller: _controller,
        children: <Widget>[
           //new DayPage(),
          new SimpleTimeSeriesChart(),
          new MonthPage(_index),
          new YearPage(_index),
          new TotalPage(_index),
          
        ],
      ),
      // bottomNavigationBar: new BottomNavigationBar(
      //     currentIndex: _index,
      //     onTap: (int _index) {
      //       setState(() {
      //         this._index = _index;
      //       });
      //     },
      //     items: <BottomNavigationBarItem>[
      //       new BottomNavigationBarItem(
      //         icon: new Icon(Icons.home),
      //         title: new Text("Home"),
      //       ),
      //       new BottomNavigationBarItem(
      //         icon: new Icon(Icons.favorite),
      //         title: new Text("Favorites"),
      //       ),
      //     ]),
    );
  }
}




// class Home extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return new Container(
      
//       child: new DefaultTabController(
//         length: 3,
//          child: new Scaffold(
      
//           appBar: new TabBar(
//             indicatorColor: Colors.amberAccent,
//             unselectedLabelColor: Colors.amberAccent,
//             labelColor: Colors.red,
//              tabs: [
//                 new Tab(icon: new Icon(Icons.directions_car)),
//                 new Tab(icon: new Icon(Icons.directions_transit)),
//                 new Tab(icon: new Icon(Icons.directions_bike)),
//               ],
           
//            // title: new Text('Tabs Demo'),
//           ),
//           body: new TabBarView(
//             children: [
//               new Icon(Icons.directions_car),
//               new Icon(Icons.directions_transit),
//               new Icon(Icons.directions_bike),
//             ],
//           ),
//         ),
//         // child: new TabBar(
//         //       tabs: [
//         //         new Tab(icon: new Icon(Icons.directions_car)),
//         //         new Tab(icon: new Icon(Icons.directions_transit)),
//         //         new Tab(icon: new Icon(Icons.directions_bike)),
//         //       ],
//         //     ),
//         // child: new Scaffold(
//         //   appBar: new AppBar(
            
//         //     title: new Text('Tabs Demo'),
//         //   ),
//         //   body: new TabBarView(
//         //     children: [
//         //       new Icon(Icons.directions_car),
//         //       new Icon(Icons.directions_transit),
//         //       new Icon(Icons.directions_bike),
//         //     ],
//         //   ),
//         // ),
//       ),

//     );
   
//   }
 //}
 //}