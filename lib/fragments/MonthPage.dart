import 'package:flutter/material.dart';

class MonthPage extends StatelessWidget {
  final int index;

  MonthPage(this.index);

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Text('MonthPage, index: $index'),
    );
  }
}