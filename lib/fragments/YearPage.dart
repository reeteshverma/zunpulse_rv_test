import 'package:flutter/material.dart';

class YearPage extends StatelessWidget {
  final int index;

  YearPage(this.index);

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Text('YearPage, index: $index'),
    );
  }
}