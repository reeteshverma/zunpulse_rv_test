import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// class IconRenderer extends charts.SymbolRenderer {
//   final IconData iconData;

//   IconRenderer(this.iconData);

//   @override
//   Widget build(BuildContext context,
//       {Size size, Color color, bool isSelected}) {
//     return new SizedBox.fromSize(
//         size: size, child: new Icon(iconData, color: color, size: 12.0));
//   }
// }

class SimpleTimeSeriesChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var data = [
      new TelemetryDateTime(new DateTime(2018, 4, 25), 10, Colors.red),
      new TelemetryDateTime(new DateTime(2018, 4, 26), 15, Colors.purple),
      new TelemetryDateTime(new DateTime(2018, 4, 27), 25, Colors.blue),
      new TelemetryDateTime(new DateTime(2018, 4, 28), 38, Colors.yellow),

      // new TelemetryDateTime(new DateTime(2018, 4, 29), 25,Colors.red),
      // new TelemetryDateTime(new DateTime(2018, 4, 30), 35,Colors.purple),
      // new TelemetryDateTime(new DateTime(2018, 5, 1), 30,Colors.blue),
      // new TelemetryDateTime(new DateTime(2018, 5, 2), 50,Colors.yellow),
      // new TelemetryDateTime(new DateTime(2018, 5, 3), 100,Colors.lightBlueAccent),
      // new TelemetryDateTime(new DateTime(2018, 5, 4),120,Colors.orange),
      // new TelemetryDateTime(new DateTime(2018, 5,5 ), 75,Colors.green),
    ];
    var series = [
      new charts.Series(
        seriesCategory:"time Series Chart",
        id: 'Sales',
        colorFn: (TelemetryDateTime tdt, _) => tdt.color,
        domainFn: (TelemetryDateTime tdt, _) => tdt.time,
        measureFn: (TelemetryDateTime tdt, _) => tdt.tdt,
        //dashPattern:[5,6,7,8,9],
          measureOffsetFn: (TelemetryDateTime tdt, _)=> 0,
        insideLabelStyleAccessorFn: (TelemetryDateTime tdt, _) =>
            new charts.TextStyleSpec(fontSize: 1022),
         labelAccessorFn:(TelemetryDateTime tdt,_)=>"34",
        // fillPatternFn:(TelemetryDateTime tdt,_)=>charts.FillPatternType.forwardHatch,

         //overlaySeries: true,
        // measureUpperBoundFn:(TelemetryDateTime tdt,_)=>tdt.tdt,
        // measureLowerBoundFn:(TelemetryDateTime tdt,_)=>30,
        //radiusPxFn:(TelemetryDateTime tdt,_)=>30 ,
        //strokeWidthPxFn:(TelemetryDateTime tdt,_)=>30,
        data: data,
      )
    ];
    var chart = new charts.TimeSeriesChart(series,

        //dateTimeFactory: const charts.LocalDateTimeFactory(),
        animate: true,
        //defaultRenderer: new charts.LineRendererConfig(symbolRenderer:new IconRenderer(Icons.cloud)),
        // secondaryMeasureAxis: new charts.NumericAxisSpec(),
        defaultInteractions: true,
        
        primaryMeasureAxis: new charts.NumericAxisSpec(
            tickProviderSpec:
                new charts.BasicNumericTickProviderSpec(zeroBound: false),
            renderSpec: new charts.SmallTickRendererSpec(
              labelStyle: new charts.TextStyleSpec(
                  fontSize: 10, // size in Pts.
                  color: charts.MaterialPalette.black),
              // Tick and Label styling here.
              lineStyle:
                  new charts.LineStyleSpec(color: charts.MaterialPalette.black),
            )),
        // primaryMeasureAxis: new charts.NumericAxisSpec(
        //       tickFormatterSpec: new charts.BasicNumericTickFormatterSpec(
        //           new NumberFormat.compactSimpleCurrency())),
        domainAxis: new charts.DateTimeAxisSpec(
            renderSpec: new charts.SmallTickRendererSpec(
                labelStyle: new charts.TextStyleSpec(fontSize: 10)),
            tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
                day: new charts.TimeFormatterSpec(
                    format: 'd', transitionFormat: 'MM-dd-yyyy'))),
        //  domainAxis: new charts.OrdinalAxisSpec(
        //     renderSpec: new charts.SmallTickRendererSpec(

        //         // Tick and Label styling here.
        //         labelStyle: new charts.TextStyleSpec(
        //             fontSize: 18, // size in Pts.
        //             color: charts.MaterialPalette.black),

        //         // Change the line colors to match text color.
        //         lineStyle: new charts.LineStyleSpec(
        //             color: charts.MaterialPalette.black))),

        /// Assign a custom style for the measure axis.
        // primaryMeasureAxis: new charts.NumericAxisSpec(
        //     renderSpec: new charts.GridlineRendererSpec(

        //         // Tick and Label styling here.
        //         labelStyle: new charts.TextStyleSpec(
        //             fontSize: 18, // size in Pts.
        //             color: charts.MaterialPalette.black),

        //         // Change the line colors to match text color.
        //         lineStyle: new charts.LineStyleSpec(
        //             color: charts.MaterialPalette.black))),

        behaviors: [
          //  new charts.SeriesLegend(
      
          //  ),
          //  new charts.RangeAnnotation([
          //   // new charts.RangeAnnotationSegment(
          //   //     new DateTime(2018, 4, 28),
          //   //     new DateTime(2018, 5, 15),
          //   //     charts.RangeAnnotationAxisType.domain),
          // ]),
          // Optional - Configures a [LinePointHighlighter] behavior with a
          // vertical follow line. A vertical follow line is included by
          // default, but is shown here as an example configuration.
          new charts.LinePointHighlighter(
              selectionModelType: charts.SelectionModelType.info,
              showHorizontalFollowLine: true,
              defaultRadiusPx: 6.0,
              radiusPaddingPx: 8.0,
              showVerticalFollowLine: true),
          // Optional - By default, select nearest is configured to trigger
          // with tap so that a user can have pan/zoom behavior and line point
          // highlighter. Changing the trigger to tap and drag allows the
          // highlighter to follow the dragging gesture but it is not
          // recommended to be used when pan/zoom behavior is enabled.
          new charts.SelectNearest(
              eventTrigger: charts.SelectNearestTrigger.tapAndDrag)
        ]);
    //dateTimeFactory: const charts.
    var chartWidget = new Padding(
      padding: new EdgeInsets.all(12.0),
      child: new SizedBox(
        height: 400.0,
        child: chart,
      ),
    );
    return new Scaffold(
      body: new Column(
        children: <Widget>[
          chartWidget,
        ],
      ),
    );
  }
}

/// Sample time series data type.
class TelemetryDateTime {
  final DateTime time;
  final int tdt;
  final charts.Color color;

  TelemetryDateTime(this.time, this.tdt, Color color)
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
