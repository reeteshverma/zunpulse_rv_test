import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
//import 'package:firebase_auth/firebase_auth.dart';
//import 'package:contact_picker/contact_picker.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'screens/Login.dart';
import 'screens/SignupPage.dart';
//import 'screens/PhoneNumberSignInPage .dart';

void main() {
  runApp(new MyApp());
  //   final user = await _auth.currentUser();
  // if (user != null) runApp(MyApp());
  // else runApp(Login());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      home: new SplashScreen(),
      //home: new Login(),
      // routes: <String, WidgetBuilder>{
      //   '/settings': (BuildContext context) => new SettingsPage(),
      // },
    );
  }
}

class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
        final FirebaseAuth _auth = FirebaseAuth.instance;
         final reference = FirebaseDatabase.instance.reference().child('users');
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  void handleTimeout() {
  //_ensureLoggedIn();
     Navigator.of(context).pushReplacement(
         new MaterialPageRoute(builder: (BuildContext context) => new Login()));
  }
  Future<Null> _ensureLoggedIn() async {
  GoogleSignInAccount user = googleSignIn.currentUser;
  print("signin_user$user");
  if (user == null)
    user = await googleSignIn.signInSilently();
      print("signin_use11111r$user.");
      reference.push().set({
      //new
      'userId': googleSignIn.currentUser.id,
      'email': googleSignIn.currentUser.email, //new
      'userName': googleSignIn.currentUser.displayName, //new
      'photoUrl': googleSignIn.currentUser.photoUrl,
      'medium': "google", //new
    });

      
  if (user == null) {
    // await googleSignIn.signIn();
    //   print("signin_use22222r$user");
     Navigator.of(context).pushReplacement(
         new MaterialPageRoute(builder: (BuildContext context) => new Login()));
  }
}

  startTimeout() async {
    var duration = const Duration(seconds: 3);
    return new Timer(duration, handleTimeout);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // _iconAnimationController = new AnimationController(
    //     vsync: this, duration: new Duration(milliseconds: 2000));

    // _iconAnimation = new CurvedAnimation(
    //     parent: _iconAnimationController, curve: Curves.easeIn);
    // _iconAnimation.addListener(() => this.setState(() {}));

    // _iconAnimationController.forward();

    startTimeout();
  }

  @override
  Widget build(BuildContext context) {
    
    return new DecoratedBox(
      decoration: new BoxDecoration(
          image: new DecorationImage(
        image: new AssetImage("assets/images/splash.jpg"),
        fit: BoxFit.fill,
      )),
    );
  }
}
class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('TestProject'),
      ),
      
    );
  }
}
