import 'package:flutter/material.dart';

import '../services/Authentication.dart';
 

 class SignupPage extends StatefulWidget{

   @override
   _SignupPageState createState()=>new _SignupPageState();
   
      
    }
   
   class _SignupPageState extends State<SignupPage> {
     //For storing data of sign up page 
     final GlobalKey<FormState> _formKey=new GlobalKey<FormState>();
     final GlobalKey<ScaffoldState> _scaffoldKey=new GlobalKey<ScaffoldState>();
     UserData newUser=new UserData();
     UserAuth userAuth=new UserAuth();     
  


     @override
     Widget build(BuildContext context)
     {
       return new Scaffold(
         body: new Padding(
         padding: const EdgeInsets.only(top: 80.0),
         child: new Column(
           crossAxisAlignment: CrossAxisAlignment.center,
        
           children: <Widget>[
            new Text("Sign up",style: new TextStyle(fontSize: 25.0),
            ),
            new Padding(
              padding: new EdgeInsets.only(top: 20.0),
            ),

            new Text("Start using Zunpulse",style: new TextStyle(fontSize: 35.0)
             ),
             new Container(
            padding: const EdgeInsets.all(25.0),
            child:new Form(
              autovalidate: true,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new TextFormField(
                    decoration: new InputDecoration(labelText: "User name"),
              
                  ),
                  new TextFormField(
                    decoration: new InputDecoration(labelText: "Email id"),
              
                  ),
                  new TextFormField(
                    decoration: new InputDecoration(labelText: "Mobile number"),
              
                  ),
                  new TextFormField(
                    decoration: new InputDecoration(labelText: "Password"),
              
                  ),
                  new TextFormField(
                    decoration: new InputDecoration(labelText: "Confirm passowrd"),
              
                  ),
                  new Padding(
                    padding: const EdgeInsets.all(20.0)
                  ),
              
                  new MaterialButton(
                    onPressed: null,
                    height: 40.0,
                    minWidth: 150.0,
                     child: new Text("Continue",style: new TextStyle(fontSize: 25.0, color: Colors.white,)),
                    color: Colors.teal[400],
                    splashColor: Colors.lightGreen,
                    textColor: Colors.white,
                  )
                ],
              )
              
            )


             )
              ],
             
       

           
         ),

         ),
         
         
       );
     }
}

 