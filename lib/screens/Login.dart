import 'dart:async';
//**************************import For FIREBASE AUTHENTICATION Package********************//
import 'package:firebase_auth/firebase_auth.dart';
//==================================================================================//
// **************************Package For GOOGLE SIGN IN*****************************//
import 'package:google_sign_in/google_sign_in.dart';
//===================================================================================//
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';


import '../model/incomingModal.dart';
import '../model/loginModel.dart';
import '../pages/DashboardPage.dart';
import 'package:zun_pulse/model/Usermodel.dart';

import '../services/Authentication.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

import 'AddDevicePage.dart';
import 'SignupPage.dart';

//import 'package:flutter_facebook_login/flutter_facebook_login.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class Login extends StatefulWidget {
  @override
  State createState() => new LoginPageState();
}

class LoginPageState extends State<Login> with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  DatabaseReference itemRef;

  List<LoginModal> loginlists = new List();
  List<IncomingModal> incominglists;
  //final reference = FirebaseDatabase.instance.reference().child('users');
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  UserData newUser = new UserData();
  UserAuth userAuth = new UserAuth();
  LoginModal loginModal;
  IncomingModal incomingModal;
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  
  static final FacebookLogin facebookSignIn = new FacebookLogin();
// Firebase Google Sign in login *************************************//
  Future<FirebaseUser> _googleSignIn() async {
    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    GoogleSignInAuthentication gSA = await googleSignInAccount.authentication;

    FirebaseUser firebaseUser = await _auth.signInWithGoogle(
        idToken: gSA.idToken, accessToken: gSA.accessToken);
    loginModal = new LoginModal(
        googleSignIn.currentUser.id,
        googleSignIn.currentUser.email,
        googleSignIn.currentUser.displayName,
        googleSignIn.currentUser.photoUrl,
        "google");
      itemRef.push().set(loginModal.toJson());
    // reference.push().set({
    //   //new
    //   'userId': googleSignIn.currentUser.id,
    //   'email': googleSignIn.currentUser.email, //new
    //   'userName': googleSignIn.currentUser.displayName, //new
    //   'photoUrl': googleSignIn.currentUser.photoUrl,
    //   'medium': "google", //new
    // });

    return firebaseUser;
  }

  Future<Null> _facebookSignIn() async {
    final FacebookLoginResult result = await facebookSignIn
        .logInWithReadPermissions(['public_profile', 'email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        print('''
         Logged in!
         
         Token: ${accessToken.token}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
        
         ''');
        loginModal = new LoginModal(accessToken.userId, "", "", "", "facebook");
        itemRef.push().set(loginModal.toJson());
        // reference.push().set({
        //   //new
        //   'userId': accessToken.userId,
        //   'medium': "facebook", //new
        // });
        break;
      case FacebookLoginStatus.cancelledByUser:
        // _showMessage('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        // _showMessage('Something went wrong with the login process.\n'
        //     'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }

  void _handleSubmitted() {
    final FormState form = _formKey.currentState;

    form.save();
    print("email/password:${newUser.email}");

    userAuth.createUser(newUser);
  }

  //*************************Create method For go On Sign up page ************************************//
  void _goSignupPage() {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new DashboardPage()));
    // Navigator.of(context).pushNamed(signuproutename);
  }

  //*****************************************END GOOGLE SIGN  IN***************************************//

  

  @override
  void initState() {
    itemRef = FirebaseDatabase.instance.reference().child('users');
    //itemRef = database.reference().child('items');
    itemRef.onChildAdded.listen(_onEntryAdded);
    // FirebaseDatabase.instance.reference().child('incoming').child("telemetry").once().then((DataSnapshot snapshot) {
    //  // print('Connected to second database and read ${snapshot.value}');
    //   Map myMap = snapshot.value;
    //   myMap.forEach((k,v) => print('reetesh_test${k}: ${v}'));
    

    //  print("finally_database :$myMap.length.toString()");
  
    FirebaseDatabase.instance.reference().child('incoming').child("telemetry").once().then((onValue) {
     Map myMap = onValue.value;
     
    //  print("finally_database :$myMap ");
    //   var titles = myMap.values;
    // List yearList = new List();
    // for (var items in titles) {
    //   yearList.add(items['latest']);
     
    // }
    var ci = new UserModel.fromJson(myMap); 
 
// var videos = data['items'];
// for (var items in videos['snippet']){
//       print(items);
    
//     final items = (ci['items'] as List).map((i) => new UserModel.fromJson(i));
// for (final item in items) {
//   print(item.id);

    //incominglists.add(new IncomingModal.fromSnapshot(snapshot));
      

      print("incoming_data_firebase${incominglists.length}");
    //  print("yearlist_databas_latest :$yearList ");

     });
     // loginlists.add(new LoginModal.fromSnapshot(event.snapshot));
      //  loginModal
      // print("yearlist_databas_latest :$yearList ");

 
    // for (var value in value.values) {
    //   myList.add(value);
    // }
    // print(myList);

    super.initState();
    new Future.delayed(const Duration(seconds: 4000));
    _iconAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _iconAnimation = new CurvedAnimation(
      parent: _iconAnimationController,
      curve: Curves.bounceOut,
    );
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

_onEntryAdded(Event event) {
    setState(() {
      incominglists.add(new IncomingModal.fromSnapshot(event.snapshot));
      for(var a in incominglists)
      {
        print("heyfinddata$a");
      }

      
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      //appBar: new AppBar(title: new Text('login'),),
      //backgroundColor: Colors.white,
      body: new Stack(fit: StackFit.expand, children: <Widget>[
        new Image(
          image: new AssetImage("assets/images/background.jpg"),
          fit: BoxFit.fill,

          // colorBlendMode: BlendMode.darken,
          // color: Colors.black87,
        ),
        new Theme(
          data: new ThemeData(
              brightness: Brightness.dark,
              inputDecorationTheme: new InputDecorationTheme(
                // hintStyle: new TextStyle(color: Colors.blue, fontSize: 20.0),
                labelStyle: new TextStyle(color: Colors.white, fontSize: 20.0),
              )),
          isMaterialAppTheme: true,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // new FlutterLogo(
              //   size: _iconAnimation.value * 140.0,
              // ),
              new Container(
                padding: new EdgeInsets.only(right: 250.0),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Text("Welcome",
                        style: new TextStyle(
                          fontSize: 25.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        )),
                    new Text(
                      "       Sign in to continue",
                      style: new TextStyle(fontSize: 15.0, color: Colors.white),
                    ),
                  ],
                ),
              ),

              new Container(
                padding: const EdgeInsets.all(40.0),
                child: new Form(
                  key: _formKey,
                  autovalidate: true,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new TextFormField(
                        decoration: new InputDecoration(
                            labelText: "Enter Email", fillColor: Colors.white),
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (String email) {
                          print("user_name:$email");
                          newUser.email = email;
                        },
                      ),
                      new TextFormField(
                        decoration: new InputDecoration(
                            labelText: "Enter Password",
                            fillColor: Colors.white),
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        onSaved: (String password) {
                          newUser.password = password;
                          print("Password : $password");
                        },
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new FlatButton(
                            child: new Text(
                              "Forgot password?",
                              style: new TextStyle(
                                  fontSize: 15.0, color: Colors.white),
                            ),
                            //onPressed: () => onPressed("/SignUp"),
                            // buttonTextStyle: buttonTextStyle),
                          )
                        ],
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(top: 40.0),
                      ),
                      new MaterialButton(
                          height: 40.0,
                          child: new Text("G + login",
                              style: new TextStyle(
                                  fontSize: 22.0, color: Colors.white)),
                          minWidth: 300.0,
                          color: Colors.red[900],
                          splashColor: Colors.teal,
                          textColor: Colors.white,
                          //child: new Icon(FontAwesomeIcons.signInAlt),
                          onPressed: () => _googleSignIn()
                              .then((FirebaseUser firebaseUser) =>
                                  print(firebaseUser))
                              .catchError((e) => print(e))),
                      new Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                      ),
                      new MaterialButton(
                          height: 40.0,
                          child: new Text("Fb login",
                              style: new TextStyle(
                                  fontSize: 22.0, color: Colors.white)),
                          minWidth: 300.0,
                          color: Colors.blue[900],
                          splashColor: Colors.teal,
                          textColor: Colors.white,
                          //child: new Icon(FontAwesomeIcons.signInAlt),
                          onPressed: () =>
                              _facebookSignIn() //.then((FirebaseUser firebaseUser)=>print(firebaseUser)).catchError((e)=>print(e))
                          ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 10.0),
                      ),
                      new MaterialButton(
                        height: 40.0,
                        child: new Text("Login",
                            style: new TextStyle(
                                fontSize: 22.0, color: Colors.white)),
                        minWidth: 300.0,
                        color: Colors.teal[400],
                        splashColor: Colors.teal,
                        textColor: Colors.white,
                        //child: new Icon(FontAwesomeIcons.signInAlt),
                        onPressed: _handleSubmitted,
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new FlatButton(
                            onPressed: () => _goSignupPage(),
                            child: new Text(
                              "New User ? Signup",
                              style: new TextStyle(
                                  fontSize: 15.0, color: Colors.white),
                            ),
                            //onPressed: () => onPressed("/SignUp"),
                            // buttonTextStyle: buttonTextStyle),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }
}

