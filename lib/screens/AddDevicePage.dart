import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import '../services/Authentication.dart';

class AddDevicePage extends StatefulWidget {
  @override
  State createState() => new AddDeviceState();
}

class AddDeviceState extends State<AddDevicePage> {
  final reference =
      FirebaseDatabase.instance.reference().child('deviceprofile');
  
  AddDeviceData addDeviceData = new AddDeviceData();
  String _value;
  String _deviceId = "";

  final TextEditingController c1 = new TextEditingController();

  List<String> _values = new List<String>();
  @override
  void initState() {
    super.initState();
    _values.addAll(["PP"]);
    _value = _values.elementAt(0);
    print("value of first position$_value");
  }

  //create method For changing dropDownbutton value with position
  void _onChanged(String value) {
    setState(() {
      _value = value;
       print("value of first position_onsubmitted$_value");
    });
  }

  void _handleSubmitted() {
      print("device_id=====${addDeviceData.deviceId }");
      print("value of first position_onsubmitted=====$_value");

       reference.push().set({
      //new
      'deviceId': _deviceId,
      'deviceType': _value, //new
      
   
    });
      
  }

  @override
  Widget build(BuildContext context) {
    Drawer drawer=new Drawer();
    return new Scaffold(drawer: drawer,
        body: new Stack(fit: StackFit.expand, children: <Widget>[
      new Image(
        image: new AssetImage("assets/images/background.jpg"),
        fit: BoxFit.fill,

        // colorBlendMode: BlendMode.darken,
        // color: Colors.black87,
      ),
      new Theme(
        data: new ThemeData(
          
            brightness: Brightness.dark,
            inputDecorationTheme: new InputDecorationTheme(
              hintStyle: new TextStyle(color: Colors.blue, fontSize: 20.0),
              labelStyle: new TextStyle(color: Colors.white, fontSize: 20.0),
            )),
        isMaterialAppTheme: true,
        child: new Padding(
          padding: const EdgeInsets.all(40.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new TextField(
                controller: c1,
                maxLines: 1,
                decoration: new InputDecoration(
                    labelText: "Enter Device id",
                    fillColor: Colors.white,
                    hintStyle: new TextStyle(fontSize: 15.0)),
                keyboardType: TextInputType.text,
                onChanged: (String deviceid){
                 _deviceId=deviceid;
                  addDeviceData.deviceId = deviceid;
                  print("device_id:$_deviceId");
                },
                
                // onSaved: (String deviceid) {
                //   // newUser.email = email;
                //   addDeviceData.deviceId = deviceid;
                //   print("device_id:$deviceid");
                // },
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 20.0),
              ),
              new Container(
                width: 340.0,
                height: 40.0,
                color: Colors.grey[100],
                child: new DropdownButton(
                 
                  value: _value,
                  items: _values.map((String value) {
                    return new DropdownMenuItem(
                      value: value,
                      child: new Text(
                        "$value",
                        style: new TextStyle(
                            fontSize: 15.0, color: Colors.amberAccent),
                        textAlign: TextAlign.center,
                      ),
                    );
                  }).toList(),
                  onChanged: (String value) {
                    _onChanged(value);
                  },
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 20.0),
              ),
              new MaterialButton(
                height: 40.0,
                child: new Text("Add Device",
                    style: new TextStyle(fontSize: 22.0, color: Colors.white)),
                minWidth: 250.0,
                color: Colors.teal[400],
                splashColor: Colors.teal,
                textColor: Colors.white,
                //child: new Icon(FontAwesomeIcons.signInAlt),
                onPressed: _handleSubmitted,
              ),
            ],
          ),
        ),
      ),
    ]));
  }
}
