import 'dart:convert';

class UserModel extends Converter<Map, Iterable<UserModel>> {
  String key;
  String userId;
  UserModel(this.key, this.userId);

  toJson() {
    return {
      "userId": userId,
    };
  }

  getDataFromFirebase() {}

  UserModel.fromJson(Map json) {
    this.key = json['key'];
    this.userId = json['userId'];
  }

  List<UserModel> getList(String jsonString) {
    //Map decodedMap = JSON.decode('{"framework":"Flutter", "awesome":true}');
    //List decodedList = JSON.decode('["Flutter", true]');
    List<UserModel> userList = new List<UserModel>();
    //  List<UserModel> list = json.decode(jsonString);
  //    int id = decoded['id'];
  // print(id.toString());
 
  // for (var word in decoded['across']) {
  //   print(word['number'].toString());
  //   print(word['word']);
  // }

// class Crossword {
//   final int id;
//   final String name;
//   final Across across;
 
//   Crossword(this.id, this.name,this.across);
// }
 
// class Across {
//   final List<Word> words;
 
//   const Across(this.words);
// }
 
// class Word {
//   final int number;
//   final String word;
 
//   const Word(this.number, this.word);
// }
 // We check it's working
 // print(crossword.name);
 
  // Crossword is loaded from JSON, do what you want with it now :
// Crossword _parseJsonForCrossword(String jsonString) {
//   Map decoded = JSON.decode(jsonString);
 
//   List<Word> words = new List<Word>();
//   for (var word in decoded['across']) {
//     words.add(new Word(word['number'], word['word']));
//   }
  
//   return new Crossword(decoded['id'], decoded['name'], new Across(words));
//}
    return userList;
  }

  UserModel getSingleData(String key) {
    UserModel userModel = new UserModel("", "");
    return userModel;
  }

  // static Map _toMap(Item i) =>  {"label": i.label, "value": i.value};

  @override
  Iterable<UserModel> convert(Map input) {
    // TODO: implement convert
     return input.keys.map((id)=> new UserModel(
       input[id]['epochtime'],
       input[id]['val_0'],
       
      )
   );
  }

  

}
