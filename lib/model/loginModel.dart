import 'package:firebase_database/firebase_database.dart';

class LoginModal {
  String key;
  String userId;
  String email;
  String userName;
  String photoUrl;
  String medium;

  LoginModal(
      this.userId, this.email, this.userName, this.photoUrl, this.medium);

  LoginModal.fromSnapshot(DataSnapshot snapshot)
  : key = snapshot.key,
        userId = snapshot.value["userId"],
        email = snapshot.value["email"];

  toJson() {
    return {
      "userId": userId,
      "email": email,
      "userName": userName,
      "photoUrl": photoUrl,
      "medium": medium
    };
  }
}