import 'package:firebase_database/firebase_database.dart';

class IncomingModal {
  String key;
  String authToken;
  String dayofweek;
  String devicetype;
  String epochtime;
  String seconds;
  String val_0;
  String val_1;
  String val_2;
  String val_3;
  String val_4;


  IncomingModal(
      this.authToken, this.dayofweek, this.devicetype, this.epochtime, this.seconds,this.val_0,this.val_1,this.val_2,this.val_3,this.val_4);

  IncomingModal.fromSnapshot(DataSnapshot snapshot)
  : key = snapshot.key,
        authToken = snapshot.value["authToken"],
        dayofweek = snapshot.value["dayofweek"],
        devicetype = snapshot.value["devicetype"],
        epochtime = snapshot.value["epochtime"],
        seconds = snapshot.value["seconds"],
        val_0 = snapshot.value["val_0"],
        val_1 = snapshot.value["val_1"],
        val_2 = snapshot.value["val_2"],
        val_3 = snapshot.value["val_3"],
        val_4 = snapshot.value["val_4"];

 
  }
