import 'package:flutter/material.dart';
import '../fragments/AddDeviceZunpulsePage.dart';
import '../fragments/SignoutPage.dart';
import '../fragments/home.dart';


class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}

class DashboardPage extends StatefulWidget {
  final drawerItems = [
    new DrawerItem("Home", Icons.home),
    new DrawerItem("Add Zunpulse Device", Icons.devices),
    new DrawerItem("Sign out", Icons.power_settings_new)
  ];

  @override
  State<StatefulWidget> createState() {
    return new DashboardPageState();
  }
}

class DashboardPageState extends State<DashboardPage> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new Home();
      case 1:
        return new AddDeviceZunpulsePage();
      case 2:
        return new SignoutPage();

      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> drawerOptions = [];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(new ListTile(
        leading: new Icon(d.icon),
        title: new Text(d.title),
        selected: i == _selectedDrawerIndex,
        onTap: () => _onSelectItem(i),
      
      )
      );
    }

    return new Scaffold(
      appBar: new AppBar(
        // here we display the title corresponding to the fragment
        // you can instead choose to have a static title
        title: new Text(widget.drawerItems[_selectedDrawerIndex].title),
      ),
      drawer: new Drawer(
        child: new Column(
          children: <Widget>[
            // new DrawerHeader(
            //    decoration: new BoxDecoration(
            //    color: Colors.red,
            //  ),
             // padding: new EdgeInsets.all(20.0),
             // child:  new UserAccountsDrawerHeader(
             new UserAccountsDrawerHeader(
                decoration: new BoxDecoration(
               color: Colors.red,
             ),
              //margin: const EdgeInsets.only(left: 30.0),
                accountName: new Text("Zunroof",style: new TextStyle(color: Colors.white),), 
                accountEmail:new Text("zunroof.com",style:new TextStyle(color: Colors.white),),
                currentAccountPicture: new CircleAvatar(backgroundColor: Colors.brown.shade800),
                 ),
               //  ),
            
           
            new Column(children: drawerOptions)
          ],
        ),
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}
