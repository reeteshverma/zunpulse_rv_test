import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';

class UserData {
  String displayName;
  String email;
  String uid;
  String password;
  String fbToken;
  UserData(
      {this.displayName, this.email, this.uid, this.password, this.fbToken});
}
class AddDeviceData {
  String deviceId;
  String deviceType;
  
  
  AddDeviceData(
      {this.deviceId, this.deviceType,});
}

class UserAuth {
  String statusMsg = "Account Created Successfully";
  //To create new User or Registration user with Firebase
  Future<String> createUser(UserData userData) async {
    print(
        "Authentication_Create User==========>email/password:${userData.email}");
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    await firebaseAuth.createUserWithEmailAndPassword(
        email: userData.email, password: userData.password);

    return statusMsg;
  }

  //For Verify User on Login Time with firebase
  //To verify new User
  Future<String> verifyUser(UserData userData) async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    await firebaseAuth.signInWithEmailAndPassword(
        email: userData.email, password: userData.password);
    return "Login Successfull";
  }

  //For mobile number verification
  //To verify new User
  // Future<String> fbLogin (UserData userData) async{
  //   FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  //   await firebaseAuth
  //       .signInWithFacebook(accessToken: null);
  //   return "Login Successfull";
  // }

}
